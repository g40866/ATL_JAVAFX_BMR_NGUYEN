/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.g40866.bmr;

/**
 *
 * @author YGDragon
 */
public enum NiveauActivité {
    SEDENTAIRE("Sédentaire", 1.2), 
    PEU_ACTIF("Peu Actif", 1.375),
    ACTIF("Actif", 1.55), 
    FORT_ACTIF("Fort Actif", 1.725),
    EXTREMEMENT_ACTIF("Extremement Actif", 1.9);

    private String typeActivite;
    private double niveau;

    private NiveauActivité(String typeActivite, double niveau) {
        this.typeActivite = typeActivite;
        this.niveau = niveau;
    }

    public String getTypeActivite() {
        return typeActivite;
    }

    public double getNiveau() {
        return niveau;
    }

        @Override
    public String toString() {
        return typeActivite;
    }
}

