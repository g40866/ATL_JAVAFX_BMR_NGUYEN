package esi.atl.g40866.bmr;

import javafx.geometry.Insets;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author G40866
 */
public class JavaFX_BMR extends Application {

    private GridPane donnees;
    private GridPane result;
    private HBox hBox;
    private VBox root;
    private MenuBar menuBar;
    private Scene scene;
    
    private Label lbl2;
    private Label lbl3;
    private Label lbl4;
    private Label lbl8;
    private Label lbl9;
    
    private TextField tfd;
    private TextField tfd2;
    private TextField tfd3;
    private TextField tfd4;
    private TextField tfd5;
    
    private RadioButton button1;
    private RadioButton button2;
    
    private ChoiceBox choiceBox;
    
    private Button clear;
    private Button calcul;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Calcul du BMR");

        // Données
        donnees = gridPaneLeft();

        // Résultats (2ème GridPane)
        result = gridPaneRight();
        hBox = new HBox(2);
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(donnees);
        hBox.getChildren().add(result);
        root = new VBox(10);

        // Boutons  
        buttonCalcul();        
        buttonClear();

        // Vérifie l'entrée.
        checkTfd();        
        checkTfd2();      
        checkTfd3();
        
        //La barre du menu.
        menuBar();
        
        // Ajout dans le VBox.
        root.getChildren().add(menuBar);
        root.getChildren().add(hBox);
        root.getChildren().add(calcul);
        root.getChildren().add(clear);
        root.setAlignment(Pos.CENTER);
        
        // Fin
        scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    
    
    
    public GridPane gridPaneLeft() {

        GridPane données = new GridPane();

        données.setPadding(new Insets(10));
        données.setHgap(10);
        données.setVgap(5);

        Label lbl = new Label("Données");
        lbl.setUnderline(true);
        données.add(lbl, 0, 0);

        lbl2 = label("Taille (cm)");
        tfd = textField("Taille en cm", 15);
        données.add(lbl2, 0, 1);
        données.add(tfd, 1, 1);

        lbl3 = new Label("Poids (kg)");
        tfd2 = textField("Poids en kg", 15);
        données.add(lbl3, 0, 2);
        données.add(tfd2, 1, 2);
        
        lbl4 = new Label("Age (année)");
        tfd3 = textField("Age en années", 15);
        données.add(lbl4, 0, 3);
        données.add(tfd3, 1, 3);
        
        Label lbl5 = new Label("Sexe");
        données.add(lbl5, 0, 4);
        
        tGroup("Femme","Homme");

        données.add(button1, 1, 4);
        données.add(button2, 1, 4);
        GridPane.setHalignment(button2, HPos.RIGHT);
        
        Label lbl6 = new Label("Style de vie");
        choiceBox = choiceBox();
        données.add(choiceBox, 1, 5);
        données.add(lbl6, 0, 5);
        
        return données;
    }
    
    
    
    
    public GridPane gridPaneRight(){
        
        GridPane resultats = new GridPane();

        resultats.setPadding(new Insets(10));
        resultats.setHgap(10);
        resultats.setVgap(2);
        
        Label lbl7 = new Label("Résultats");
        lbl7.setUnderline(true);
        resultats.add(lbl7, 0, 0);
        
        lbl8 = new Label("BMR");
        tfd4 = textField("Résultats du BMR", 15);
        resultats.add(lbl8, 0, 1);
        resultats.add(tfd4, 1, 1);
        
        lbl9 = new Label("Calories");
        tfd5 = textField("Dépenses en calories", 15);
        resultats.add(lbl9, 0, 2);
        resultats.add(tfd5, 1, 2);
        
        
        return resultats;
    }

    
    
    
    public Label label(String lblName) {

        Label lbl = new Label(lblName);

        return lbl;
    }

    
    
    
    public TextField textField(String txtField, int colCount) {

        TextField txfd = new TextField();
        txfd.setPromptText(txtField);
        txfd.setPrefColumnCount(colCount);

        return txfd;
    }
    
    
    
    
    public void tGroup(String text1, String text2){
        
        ToggleGroup tGroup = new ToggleGroup();
        button1 = new RadioButton(text1);
        button1.setToggleGroup(tGroup);
        button1.setSelected(true);
        button2 = new RadioButton(text2);
        button2.setToggleGroup(tGroup);
        
    }

    
    
    public ChoiceBox choiceBox (){

        ChoiceBox chBox = new ChoiceBox();
        chBox.getItems().addAll(NiveauActivité.values());
        chBox.getSelectionModel().selectFirst();
        
        return chBox;
    }
    
    
    
    
    public void buttonCalcul(){
        
        calcul = new Button("Calcul du BMR");
        calcul.setMaxWidth(Double.MAX_VALUE);
        calcul.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                double calorie ;
                double bmr ;
                if (tfd2.getText().equals("") || tfd.getText().equals("")
                        || tfd3.getText().equals("") 
                        || Double.parseDouble(tfd2.getText()) == 0
                        || Double.parseDouble(tfd.getText()) == 0
                        || Integer.parseInt(tfd3.getText()) == 0) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, 
                            "Veuillez entrer une valeur plus grande que 0");
                    alert.showAndWait();
                } else {
                    double poids = Double.parseDouble(tfd2.getText());
                    double taille = Double.parseDouble(tfd.getText());
                    int age = Integer.parseInt(tfd3.getText());
                    if (button2.isSelected()) {
                        bmr = (13.7 * poids) + (5 * taille) - (6.8 * age) + 66;
                    } else {
                        bmr = (9.6 * poids) + (1.8 * taille) 
                                - (4.7 * age) + 655;
                    }
                    NiveauActivité act = (NiveauActivité) 
                            choiceBox.getSelectionModel().getSelectedItem();
                    calorie = act.getNiveau() * bmr;
                    tfd4.setText(Double.toString(bmr));
                    tfd5.setText(Double.toString(calorie));
                }
            }
        });
    }

    
    
    
    
    public void buttonClear() {

        clear = new Button("Clear");
        clear.setMaxWidth(Double.MAX_VALUE);
        clear.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                tfd.clear();
                tfd2.clear();
                tfd3.clear();
                tfd4.clear();
                tfd5.clear();
            }
        });
    }

    
    
    
    public void menuBar() {

        menuBar = new MenuBar();
        Menu file = new Menu("File");
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });
        file.getItems().add(exit);
        menuBar.getMenus().add(file);
    }

    
    
    
    public void checkTfd() {

        tfd.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (!event.getCharacter().matches("[0-9]")) {
                    event.consume();
                }
            }
        });
    }
    
    
    
    
    public void checkTfd2() {

        tfd2.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (!event.getCharacter().matches("[0-9]")) {
                    event.consume();
                }
            }
        });
    }
    
    
    
    
    public void checkTfd3() {

        tfd3.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (!event.getCharacter().matches("[0-9]")) {
                    event.consume();
                }
            }
        });
    }
}
